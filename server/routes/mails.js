var router  = require("express").Router();
var Mail = App.model("mail");

router.get('/', function(req, res) {
	var updatedAt = new Date(parseInt(req.params.updatedAt) || 0);
    Mail.find({updatedAt: {$gt: updatedAt}}, function(err, mails) {
            if (err)
                res.send(err);

            res.send(mails.map(function(mail) {
                return mail;                 
            }));
        });
});

router.post('/', function(req, res) {
    if( req.body.storedIn === 'outbox' || req.body.storedIn === 'drafts' ){
        var mail = new Mail();      // create a new instance of the Bear model
        mail.subject = req.body.subject; 
        mail.recipient = req.body.recipient; 
        mail.sender = req.body.sender; 
        mail.message = req.body.message; 
        mail.storedIn = req.body.storedIn;
        mail.updatedAt = new Date();
        mail.attachments = req.body.attachments;
        
        mail.save(function(err){
            if( err ){
                console.error(err);
                res.send(err);
            } 
            console.log("mail added to ", mail.storedIn, " for processing: ", mail.subject);
            res.json(mail);
        });   
    } else {
        res.send({error: 'wrong store'})
    }
});


router.put('/:_id', function(req, res) {
    Mail.findById(req.params._id, function(err, mail){      // create a new instance of the Bear model
        if( err ){
            res.send(err);
        }
       
        if( !mail ){
            res.send({error: '404'})
            return;
        } 
        mail.subject = req.body.subject; 
        mail.recipient = req.body.recipient; 
        mail.sender = req.body.sender; 
        mail.message = req.body.message; 
        mail.updatedAt = Date.now();
        mail.deleted = req.body.deleted;
        mail.attachments = req.body.attachments;

        mail.save(function(err){
            if( err ){
                console.error(err);
                res.send(err);
            } 
            console.log("mail added to ", mail.storedIn, " for processing: ", mail.subject);
            res.json(mail);
        });   
    });
});

module.exports = router;

