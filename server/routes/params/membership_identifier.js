var Membership = App.model('membership');
var slug = require('slug');

module.exports = function(req, res, next, membershipIdentifier) {
	Membership.findOne({
		slug: slug(membershipIdentifier), 
		group: req.networkGroup._id, 
		network: req.network._id,
		removed: {$ne: true}	  
	}).exec((err, membership) => {
		req.membership = membership;
		next();
	});
}
