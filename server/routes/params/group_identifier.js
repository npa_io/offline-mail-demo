var NetworkGroup = App.model('network_group');
var slugg = require("slug");

module.exports = function(req, res, next, groupIdentifier) {
  NetworkGroup.findOne({slug: slugg(groupIdentifier), network: req.network._id}).exec((err, networkGroup) => {
    req.networkGroup = networkGroup;
    next();
  });
};
