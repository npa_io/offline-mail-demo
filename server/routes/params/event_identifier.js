var Event = App.model('event');

module.exports = function(req, res, next, eventIdentifier) {
  Event.findOne({slug: eventIdentifier, group: req.networkGroup._id, network: req.network._id}).exec((err, event) => {
    req.event = event;
    next();
  });
}
