var Network = App.model('network');
var slugg = require('slug');

module.exports = function(req, res, next, networkIdentifier) {
  Network.findOne({slug: slugg(networkIdentifier), owner: req.user._id}).exec((err, network) => {
    req.network = network;
    next();
  });
};
