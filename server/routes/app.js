var router  = require("express").Router();


router.get("/main.js", function(req, res){
	res.sendFile(App.appPath('public/app/main.js'));
});

router.get("/app.manifest", function(req, res){
	res.header('Content-Type', 'text/cache-manifest');
	res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
	res.header('Pragma', 'no-cache');
	res.header('Expires', '0');
	res.sendFile(App.appPath('app.manifest'));
});

router.get("/main.css", function(req, res){
	res.sendFile(App.appPath('public/app/main.css'));
});

router.get("/*", function(req, res) {
	res.render("app/app");
});

module.exports = router;
