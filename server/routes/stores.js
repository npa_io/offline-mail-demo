var router  = require("express").Router();
var Mail = App.model("mail");

router.get('/:storeName', function(req, res) {
    Mail.find({storedIn: req.params.storeName}, function(err, mails) {
            if (err)
                res.send(err);

            res.send(mails.map(function(mail) {
                return mail;                 
            }));
        });
});

router.post('/:storeName', function(req, res) {
    if( req.params.storeName === 'outbox' || req.params.storeName === 'drafts' ){
        var mail = new Mail();      // create a new instance of the Bear model
        mail.subject = req.body.subject; 
        mail.recipient = req.body.recipient; 
        mail.sender = req.body.sender; 
        mail.message = req.body.message; 
        mail.storedIn = req.params.storeName;
        mail.updatedAt = new Date();
        
        mail.save(function(err){
            if( err ){
                console.error(err);
                res.send(err);
            } 
            console.log("mail added to ", mail.storedIn, " for processing: ", mail);
            res.json(mail);
        });   
    } else {
        res.send({message: 'wrong store'})
    }
});

module.exports = router;

