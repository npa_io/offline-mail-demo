var env = process.env.NODE_ENV || "develop",
	packageJson = require("../../package.json"),
	path = require("path"),
	express = require("express"),
	bodyParser = require("body-parser"),
	mongoose = require("mongoose"),
	morgan = require('morgan'),
	cors = require('cors');

console.log("starting app in " + env + " mode");

global.App = {
	app: express(),
	router: express.Router(),
	port: process.env.PORT || 3000,
	version: packageJson.version,
	name: packageJson.name,
	root: path.join(__dirname, ".."),
	appPath: function(path){
		return this.root + "/" + path;
	},
	require: function(path){
		return require(this.appPath(path));
	},
	command: function(path){
		return this.require('commands/'+path);
	},  
	middleware: function(path){
		return this.require('middlewares/'+path);
	},
	model: function(path){
		return this.require('models/'+path);
	},
	route: function(path){
		return this.require('routes/'+path);
	},
	routeParam: function(path){
		return this.require('routes/params/'+path);
	},
	env: env,
	start: function(){
		if(!this.isStarted){
			this.isStarted = true;

			this.startDB();

			this.app.listen(this.port);
			console.log("Running App version " + this.version + " on port " + this.port + " in " + this.env + " mode");
		}
	},
	startDB: function(){
		if( !this.db){
			var mongoUrl = `mongodb://localhost/awesome_offline_mail_{env}`;
			mongoose.connect(mongoUrl);
			this.db = mongoose.connection;
			this.db.on("error", console.error.bind(console, "mongodb connection error:"));
			this.db.once("open", function (callback) {
				console.log("connected to mongodb");
			});

		}

	}
};

// jade setup
App.app.set("views", App.appPath("views"));
App.app.set("view engine", "jade");
App.app.set("view options", {pretty: env === 'development'});


// Middlewares
App.app.use(morgan('combined')); // logger

App.app.use(cors());

//App.app.use('/api/*', bodyParser.json()); // for parsing application/json// 
App.app.use('/api/*',bodyParser.json({limit: '50mb'}));
//app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

/*App.app.use('/api/*', function(req, res, next){
setTimeout(next, 1000);
})*/


App.app.use(express.static(App.appPath("/public")));


// Routes
App.require('config/routes')(App.app);



// perform fake send
var Mail = App.model("mail");
var timeout = setInterval(function(){
	Mail.find({storedIn: 'outbox'}, function(err, mails){
		if( err) {
			console.log('err when finding outbox mails: ', err);
		}
		mails.forEach(function(mail){
			mail.storedIn = "sent";
			mail.date = new Date();
			mail.save(function(err){
				if(!err){
					console.log('mail '+ mail.subject + ' sent');

					if( mail.recipient === 'jakob@npapps.com' ){
						var newMail = new Mail();

						newMail.subject = mail.subject; 
						newMail.recipient = mail.recipient; 
						newMail.sender = mail.sender; 
						newMail.message = mail.message; 
						newMail.updatedAt = Date.now();
						newMail.date = Date.now();
						newMail.deleted = false;
						newMail.attachments = mail.attachments;
						newMail.storedIn = 'inbox';
						newMail.unread = true;

						newMail.save(function(err){
							if(!err){
								console.log('mail '+ newMail.subject + ' recieved in inbox');
							} else {
								console.log('save error newMail; ', err);
							}
						})




					}
				}
			});
		});
	});

}, 1000);


