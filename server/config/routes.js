module.exports = function(app){
  

  
  app.use("/app", App.route("app"));
  app.use("/api/ping", App.route("ping"));
  app.use("/api/mails", App.route("mails"));
  app.use("/api/stores", App.route("stores"));

  

};


