var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var Mail = module.exports = mongoose.model('Mail', { 
    recipient: String, 
    sender:String, 
    date:Date, 
    message:String, 
    subject: String, 
    storedIn:String, 
    unread:Boolean,
    updatedAt: Date,
    attachments: [{
        checksum: String,
        filecontent: String,
        filename: String,
        filesize: Number,
        filetype: String,

    }]
});
