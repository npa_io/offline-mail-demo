var AppDispatcher = require('../dispatcher/app_dispatcher');
var EventEmitter = require('events').EventEmitter;
var AppStateConstants = require('../constants/app_state_constants');
var assign = require('object-assign');

import {listenToAppcacheUpdates} from '../services/listen_to_appcache_changes';

var CHANGE_EVENT = 'change';


var _isOnline = false;

function changeStatus(newStatus){
	if(_isOnline != newStatus){
		_isOnline = newStatus;
		return true;
	}
}

var _isSyncing = false;
function changeSyncStatus(newStatus){
	if(_isSyncing != newStatus){
		_isSyncing = newStatus;
		return true;
	}
}


var _localDBServer;

var AppStateStore = assign({}, EventEmitter.prototype, {

	isOnline() {
		return {isOnline: _isOnline};
	},

	isSyncing(){
		return {isSyncing: _isSyncing};
	},

	localDBServer(){
		return {dbServer: _localDBServer};
	},

	currentUser(){
		return {
			name: 'Jakob Dam Jensen',
			email: 'jakob@npapps.com'
		}
	},

	emitChange: function({mail, action} = {}) {
		this.emit(CHANGE_EVENT, {mail, action});
	},

	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	},

	hasAppCacheBeenUpdated(){
		return (window.applicationCache.status == window.applicationCache.UPDATEREADY);		
	},

	dispatcherIndex: AppDispatcher.register(function(action) {

		switch(action.actionType) {
			case AppStateConstants.APP_STATE_LOCAL_DB_CONNECTED:
				if( _localDBServer !== action.dbServer ){
				_localDBServer = action.dbServer;
				AppStateStore.emitChange();
			}
			break;
			case AppStateConstants.APP_STATE_IS_ONLINE:
				if( changeStatus(true) ){
					console.log("did change online state to: online");				
					AppStateStore.emitChange();
				}
			break;

			case AppStateConstants.APP_STATE_IS_OFFLINE:
				if( changeStatus(false) ){
					console.log("did change online state to: offline");				
				AppStateStore.emitChange();
			}
			break;
			case AppStateConstants.APP_STATE_START_SYNC:
				if( changeSyncStatus(true) ){
				AppStateStore.emitChange();
			}
			break;

			case AppStateConstants.APP_STATE_STOP_SYNC:
				if( changeSyncStatus(false) ){
				AppStateStore.emitChange();
			}
			break;

			case AppStateConstants.APP_STATE_START_APP_CACHE_UPDATE_LISTENER:
				listenToAppcacheUpdates();
			break;

			case AppStateConstants.APP_STATE_START_APP_CACHE_DID_CHANGE:
				AppStateStore.emitChange();			
			break;			
		}

		return true;
	})

});


window.AppStateStore = AppStateStore;

module.exports = AppStateStore;



