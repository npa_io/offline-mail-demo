var AppDispatcher = require('../dispatcher/app_dispatcher');
var EventEmitter = require('events').EventEmitter;
var MailsConstants = require('../constants/mails_constants');
var ApiConstants = require('../constants/api_constants');
var LocalStoreConstants = require('../constants/local_db_constants');
var assign = require('object-assign');
var _ = require('underscore');

var CHANGE_EVENT = 'change';

/*
var _mails = ['creauna', 'velux', 'aarhus', 'danfoss', 'iwdk', 'FinnMortensenHuse'].map( (name, idx) => {
    return {id: idx, subject: 'mail to '+name, message: 'this is a mail to '+name, from: 'jakob@'+name+'.dk', to: 'jakob@npapps.com'};
});*/

var _stores = {
    inbox: [],
    outbox: [],
    drafts: [],
    sent: []
};

function resetStores(){
    _stores = {
       inbox: [],
        outbox: [],
        drafts: [],
        sent: []
    }
}

var mailsStoresMap = {};


function create(mail, storeName) {
    if( storeName && _stores[storeName] && mail.recipient && mail.date && mail.sender && mail.message && mail.subject ){
        _stores[storeName].push(mail);
        return true;
    }
    return false;
}

function get(id, store){
    return _.find(_stores[store], function(mail){ return parseInt(mail.id) === parseInt(id)});
}

function update(id, store, mail) {
    if( mail.recipient && mail.date && mail.sender && mail.message && mail.subject ){
        var storedMail = get(id, store);
        if( storedMail ){
            
            let updatedMail = assign(storedMail, mail);
            destroy(id, store);
            create(mail); 



            return true;

        }
        //stores[mail.storedIn].push(mail);
    }
    return false;
}

function destroy(id, storeName) {
    var storedMail = get(id, storeName),
        store = _stores[storeName];
    if( storedMail ){
        _stores[store] = _.without(store, storedMail);
        return true;
    }
    return false;
}

function move(id, fromStoreName, toStoreName){
    var storedMail = get(id, fromStoreName);

    if( storedMail ){
        _stores[fromStoreName] = _.without(_stores[fromStoreName], storedMail);
        _stores[toStoreName].push(storedMail);

        storedMail.storedIn = toStoreName;

        return true;
    }
    return false;

}

var MailStore = assign({}, EventEmitter.prototype, {

    getAll: function(store) {
        return {mails: _.sortBy(_stores[store], (mail) => -mail.date)};
    },

    get(id, store){
        return get(id, store);
    },

    emitChange: function({mail, action} = {}) {
        this.emit(CHANGE_EVENT, {mail, action});
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    dispatcherIndex: AppDispatcher.register(function(action) {
        var {mail, id, storeName, fromStoreName, toStoreName} = action;

        switch(action.actionType) {
            case LocalStoreConstants.LOCAL_STORE_CREATE_MAIL: {
                
                let mail = action.data;
                
                 _stores[mail.storedIn].push({
                        id: mail.id,
                        remoteId: mail.remoteId,
                        sender: mail.sender,
                        recipient: mail.recipient,
                        message: mail.message,
                        date: mail.date ? new Date(mail.date) : undefined,
                        hasBeenRead: mail.hasBeenRead,
                        subject: mail.subject,
                        attachments: mail.attachments
                    });

                    MailStore.emitChange();
				

                break;
            }
            
            case LocalStoreConstants.LOCAL_STORE_LOAD_MAIL: {
                
                let mails = action.data;
                resetStores();

                mails.forEach((mail) => {
                    _stores[mail.storedIn].push({
                            id: mail.id,
                            remoteId: mail.remoteId,
                            sender: mail.sender,
                            recipient: mail.recipient,
							message: mail.message,
							date: mail.date ? new Date(mail.date) : undefined,
							hasBeenRead: mail.hasBeenRead,
							subject: mail.subject,
							attachments: mail.attachments
					});

				});

				MailStore.emitChange();

				break;
			}
		}



		return true; 
	})

});

window.MailStore = MailStore;
module.exports = MailStore;

