var Api = require('../services/api.js');

var ApiActions = {
    getStoreData(storeName, entityId) {
		Api.getStoreData(storeName, entityId);
    },

    createMail(storeName, mail){
		Api.createMail(storeName, mail);
    },

    updateDraftMail(remoteId, mail){
		Api.updateDraftMail(remoteId, mail);
    }
};

module.exports = ApiActions;


