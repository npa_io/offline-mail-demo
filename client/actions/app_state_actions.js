var AppDispatcher = require('../dispatcher/app_dispatcher');
var Constants = require('../constants/app_state_constants');

var AppStateActions = {
  localDBLoaded(dbServer){
    AppDispatcher.dispatch({
      actionType: Constants.APP_STATE_LOCAL_DB_CONNECTED,
      dbServer: dbServer
    });
  },

  isOnline(){
    AppDispatcher.dispatch({
      actionType: Constants.APP_STATE_IS_ONLINE
    });
  },
  
  isOffline(){
    AppDispatcher.dispatch({
      actionType: Constants.APP_STATE_IS_OFFLINE
    });
  },

  startSync(){
     AppDispatcher.dispatch({
      actionType: Constants.APP_STATE_START_SYNC
    });
  },

  finishSync(){
    AppDispatcher.dispatch({
      actionType: Constants.APP_STATE_STOP_SYNC
    });
  },

  startCacheChangeListener(){
  	AppDispatcher.dispatch({
		actionType: Constants.APP_STATE_START_APP_CACHE_UPDATE_LISTENER
	})
  },

  appCacheUpdateStatusChanged(){
  	AppDispatcher.dispatch({
		actionType: Constants.APP_STATE_START_APP_CACHE_DID_CHANGE
	})  
  }
}
window.AppStateActions = AppStateActions;

module.exports = AppStateActions;


