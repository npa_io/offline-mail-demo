var AppStateActions = require('../actions/app_state_actions.js'),
    request = require('superagent');
var intervalId,
    isPinging,
    TIMEOUT = 10000,
    INTERVAL_PAUSE = 10000,
    url = "/api/ping";

var Pinger = {

    _connectionChecker(){
        if( window.navigator.onLine === undefined || window.navigator.onLine === true){
            if( !isPinging ){
                isPinging = true;
                request
                    .get(url)
                    .timeout(TIMEOUT)
                    .end(function(err, res){
                        if(!err){
                            AppStateActions.isOnline();
                        } else {
                            AppStateActions.isOffline();
                        }

                        isPinging = false;
                    });
            }
        } else {
            AppStateActions.isOffline();
        }
    },

    start(){
        if(!window.navigator){
            return;
        }

        if( intervalId ){
            this.stop();
        }
        this._connectionChecker();
        intervalId = setInterval(this._connectionChecker, INTERVAL_PAUSE);

        window.addEventListener('online',  () => {
            intervalId = setInterval(this._connectionChecker, INTERVAL_PAUSE);
        });
        window.addEventListener('offline', () => {
            clearInterval(intervalId);
        });
    },

    
    stop(){
        if( intervalId ){
            clearInterval(intervalId);
            intervalId = undefined;
            window.removeEventListener('online',  this._connectionChecker);
            window.removeEventListener('offline', this._connectionChecker);
        }
    }
}


module.exports = Pinger;

