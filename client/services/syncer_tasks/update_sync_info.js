module.exports = function updateSyncInfo( dbServer ) {
  return function(syncInfo) {
    var promise = undefined;

    if( syncInfo.updatedAt === 0 ) {
      promise = dbServer.syncInfo.add( {
        updatedAt: Date.now()
      } )
    } else {
      promise = dbServer.syncInfo.query().all().modify( {
        updatedAt: Date.now()
      } ).execute()
    }


    return promise;
  }
}
