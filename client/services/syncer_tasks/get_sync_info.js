var Q = require( 'q' );

module.exports = function( dbServer ) {
  var deferred = Q.defer();
  dbServer.syncInfo.query().all().execute().then( function( result ) {

    var syncInfo = {
      updatedAt: 0
    };
    if( result.length > 0 ) {
      syncInfo = result[ 0 ];
    }
    deferred.resolve( syncInfo );
    console.log('finished getting sync info');
  } );

  return deferred.promise;
}
