var Q = require('q');

module.exports = function normalizeRemoteDataAndPrepareDataStructures ( syncInfo ) {
  return function( data ) {
    return Q.fcall( function() {
      var remoteUpdatesMap = {};

      data.forEach( function( item ) {
        item.remoteId = item._id;
        item.updatedAt = new Date( item.updatedAt ).getTime()
        delete item._id

        remoteUpdatesMap[ item.remoteId ] = item;
      } );
      syncInfo.remoteUpdatesMap = remoteUpdatesMap;

      return syncInfo;
    } )
  };
}
