var Q = require('q');

module.exports = function addNewItems( dbServer, syncConfig ) {
  return function(syncInfo) {
    var deferred = Q.defer(),
        remoteIds = Object.keys( syncInfo.remoteUpdatesMap),
      data = remoteIds.map( function( remoteId ) {
        return syncInfo.remoteUpdatesMap[ remoteId ];
      } );

      
    dbServer[syncConfig.tableName].add( data ).then(function(data){
      syncInfo.newItemsAdded = data;
      deferred.resolve(syncInfo);
    });

    return deferred.promise;
  }
};
