import AppActions from '../actions/app_state_actions';
var isListening = false;


export function listenToAppcacheUpdates(){
	if( !isListening ){
		console.log('now listening for app cache updates');
		window.addEventListener('load', function(e) {

			window.applicationCache.addEventListener('updateready', function(e) {
				console.log('app cache state changed');
				AppActions.appCacheUpdateStatusChanged();
			}, false);

		}, false);
		isListening = true;
	}

	
}
