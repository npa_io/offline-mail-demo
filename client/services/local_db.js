var AppDispatcher = require('../dispatcher/app_dispatcher');
var Constants = require('../constants/local_db_constants');
var AppStateActions = require('../actions/app_state_actions');
var db = require('db.js');

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}


function startDB() {
	return db.open( {
		server: 'mega-offline-mail',
		version: 1,
		schema: {
			mails: {
				key: { keyPath: 'id', autoIncrement: true },
				indexes: {
					remoteId: { unique: true },
					updatedAt: { unique: false }
				}
			},
			syncInfo: {
				key: { keyPath: 'id', autoIncrement: true }
			}
		}
	} );
}

var _dbServer;
var dbConnection = startDB().then((server) => {
	_dbServer = server;
	AppStateActions.localDBLoaded(server)
});


function dispatch(type, data) {
	AppDispatcher.dispatch({
		actionType: type,
		data: data
	});
}

var LocalStoreActions = {
	createMail(mail) {
		mail.updatedAt = Date.now();
		mail.date = Date.now();
		mail.deleted = false;
		_dbServer.add( 'mails', mail).then((res) => {
			console.log(res[0]);
			dispatch(Constants.LOCAL_STORE_CREATE_MAIL, res[0]);
		});
	},

	loadMail(){
		_dbServer.mails.query()
		.filter((item) => item.deleted !== true)
		.execute()
		.then((results) => {
			dispatch(Constants.LOCAL_STORE_LOAD_MAIL, results);
		});
	},

	updateMail(mail) {
		mail.updatedAt = Date.now();
		dbConnection.mails.query()
		.filter( 'id', model.id )
		.modify( mail )
		.execute()
		.then((result) => {
			dispatch(Constants.LOCAL_STORE_UPDATE_MAIL, result);
		});
	},

	deleteMail(mail){
		mail.updatedAt = Date.now();
		mail.deleted = true;

		dbConnection.mails.query()
		.filter( 'id', model.id )
		.modify( mail )
		.execute()
		.then((result) => {
			dispatch(Constants.LOCAL_STORE_DELETE_MAIL, result);
		});
		AppDispatcher.dispatch({
			actionType: MailConstants.LOCAL_STORE_DELETE_MAIL,
			id: id
		});
	}
};

window.LocalStoreActions = LocalStoreActions;

module.exports = LocalStoreActions;

