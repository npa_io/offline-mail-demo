var React = require('react'),
	ConnectionStatus = require('./connection_status.jsx'),
	HeaderButton = require('./header_button.jsx'),
	AppStateStore = require('../stores/app_state_store.js'),
	Syncer = require('../services/syncer.js');



module.exports = React.createClass({
	_synchronize(){
		Syncer.sync(AppStateStore.localDBServer());
	},

	getInitialState(){
		return {isSyncing: false};
	},

	componentDidMount() {
		  AppStateStore.addChangeListener(this._onChange);

	},

	componentWillUnmount() {
		AppStateStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
		this.setState({isSyncing: AppStateStore.isSyncing().isSyncing})
	},
	

	render(){
		var syncButtonTitle = this.state.isSyncing ? "Syncing" : "Get mails";
		return (
			<header className='header'>
				<ConnectionStatus className="header-button" />
				<HeaderButton title='Compose' to='compose'/>
				<HeaderButton title={syncButtonTitle} store={AppStateStore} storeMethod={AppStateStore.isOnline} onClick={this._synchronize}/>
			</header>
			)
		
	}
});
