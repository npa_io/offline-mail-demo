var React = require( 'react' ),
	Link = require( 'react-router' ).Link,
	Icon = require( './icon.jsx' );

module.exports = React.createClass( {

	render(){
		var name = this.props.name;
		return (
			<Link to="mails" className='store-list-item' activeClassName='store-list-item--active'
				  params={{store: name}}>
				<Icon name={name}/>
                <span className='store-list-item__name'>{name}</span>
			</Link>
		)
	}
} );
