var React = require('react');

module.exports = React.createClass({

    render(){

        var name = this.props.name,
            className = "icon-"+name;
        
        return <span className={className}/>
    }
});

