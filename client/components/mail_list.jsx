var React = require( 'react' ),
	Router = require( 'react-router' ),
	Link = Router.Link,
	RouteHandler = Router.RouteHandler,
	ApiActions = require( '../services/api.js' ),
	AppStateStore = require( '../stores/app_state_store.js' ),
	MailsStore = require( '../stores/mail_store.js' ),
	moment = require( 'moment' );


module.exports = React.createClass( {

	getInitialState: function() {
		return MailsStore.getAll( this.props.storeName );
	},

	componentWillReceiveProps( nextProps ){
		if( nextProps.storeName !== this.props.storeName ) {
			this.setState( MailsStore.getAll( nextProps.storeName ) );
		}

	},

	componentDidMount() {
		MailsStore.addChangeListener( this._onChange );
	},

	componentWillUnmount() {
		MailsStore.removeChangeListener( this._onChange );
	},

	_onChange: function() {
		this.setState( MailsStore.getAll( this.props.storeName ) );
	},

	render(){
		var mailRows = [];
		
		if( this.state.mails ) {
			mailRows = this.state.mails.map( ( mail ) => {
				var {subject, date, recipient, sender} = mail;
				var name = AppStateStore.currentUser().email === sender && recipient || sender;
				var linkParams = { mailId: mail.id, store: this.props.storeName };
				var dateTime = date ? moment( new Date(date) ).format( 'MM/DD/YY HH:mm' ) : "";
				
				console.log("unread: ", mail.unread);

				return (
					<li className='mail-list-item' key={mail.id}>
						<Link to='mail' className='mail-list-item__link' activeClassName='mail-list-item__link--active'
							  params={linkParams}>
                        <span className='mail-list-item__name'>
                            {name}
                        </span>
                        <span className='mail-list-item__datetime'>
                            {dateTime}
                        </span>
                        <span className='mail-list-item__subject'>
                            {subject}
                        </span>
						</Link>
					</li>
				)
			} );
		}

		return (
			<ul className='mail-list'>
				<li className='mail-list__header'>From</li>
				{mailRows}
			</ul>
		)
	}


} );

