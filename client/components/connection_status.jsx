var React = require('react'),
    AppStateStore = require('../stores/app_state_store.js'),
    ConnectionStatusService = require('../services/api_pinger.js');
    

module.exports = React.createClass({
    
    getInitialState: function() {
		return  AppStateStore.isOnline();
    },

    componentDidMount() {
        AppStateStore.addChangeListener(this._onChange);
        ConnectionStatusService.start();
    },

    componentWillUnmount() {
        AppStateStore.removeChangeListener(this._onChange);
        ConnectionStatusService.stop();
    },

    _onChange: function() {
        this.setState(AppStateStore.isOnline());
    },


    render(){
        var status = this.state.isOnline && 'online' || 'offline',
            className = this.props.className + " connection-button";

        
        if( status === 'online' ){
            className = className + " connection-button--online";
        }

        return (
            <div className={className}>
            {status}
            </div>
        )
    }


});


