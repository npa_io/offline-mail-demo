var React = require('react'),
    Router = require('react-router'),    
    Link = Router.Link,
    RouteHandler = Router.RouteHandler,
    MailsStore = require('../stores/mail_store.js');

function getMailState(id, storeName) {
	return {
		mail: MailsStore.get(id, storeName)
	};
}

module.exports = React.createClass({


    render(){
        var {file} = this.props,
            previewClassName = this.props.className+"-image",
            isMovie = file.filecontent.startsWith("data:video/quicktime;");
        
         return (<div className={this.props.className} key={file.filename}>
                            <img className={previewClassName} src={file.filecontent}/>
                        </div>
                )


    }

});
