var React = require('react'),
Router = require('react-router'),    
Link = Router.Link,
RouteHandler = Router.RouteHandler,
FilePreviewer = require('../components/file_previewer.jsx'),
MailsStore = require('../stores/mail_store.js');

function getMailState(id, storeName) {
    return {
        mail: MailsStore.get(id, storeName)
    };
}

module.exports = React.createClass({

    getInitialState() {
        var {mailId, store} = this.props.params;
        return getMailState(mailId, store);
    },

    componentWillReceiveProps(nextProps){
        this.setState(getMailState(nextProps.params.mailId, this.props.params.store));
    },

    componentDidMount() {
        MailsStore.addChangeListener(this._onChange);
    },

    componentWillUnmount() {
        MailsStore.removeChangeListener(this._onChange);
    },

    _onChange: function() {
        this.setState(getMailState(this.props.params.mailId, this.props.params.store));
    },

    render(){
        var mail = this.state.mail;


        if(mail){
            let attachments = this.state.mail.attachments;

            if(attachments) {
                attachments = attachments.map((attachment) => {
                    return <FilePreviewer className='mail-viewer-attachments__item' file={attachment}/>
            });
        }

        return (
            <div className='mail-viewer'>
                <h1 className='mail-viewer__header'>{mail.subject}</h1>
                <div className='mail-viewer__sender'>
                    <span>
                        from:
                    </span> 
                    <span>
                        {mail.sender}
                    </span>
                </div>
                <div className='mail-viewer__recipient'>
                    <span>
                        to:
                    </span> 
                    <span>
                        {mail.recipient}
                    </span>
                </div>

                <div className='mail-viewer__datetime'>{mail.datetime}</div>

                <p className='mail-viewer__message'>
                    {mail.message}
                </p>
                <div className='mail-viewer__attachments'>
                    {attachments}
                </div>
            </div>
        )
    } else {
        return (
            <div className='mail-viewer mail-viewer--no-email-selected'>
                <h1 className='mail-viewer__no-email-selected-header'>Select email in the list</h1>
            </div>
        )
    }
}


    });




