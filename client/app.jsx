var React = require('react'),
	Inbox = require('./screens/inbox.jsx'),
	ComposeMail = require('./screens/compose_mail.jsx'),
	MailViewer = require('./screens/mail_viewer.jsx'),
	StoreNavigation = require('./components/store_list.jsx'),
	Router = require('react-router'),
	DefaultRoute = Router.DefaultRoute,
	Route = Router.Route,
	Link = Router.Link,
	RouteHandler = Router.RouteHandler,
	LocalDBService = require('./services/local_db'),
	Syncer = require('./services/syncer'),
	Header = require('./components/header.jsx'),
	AppStateStore = require('./stores/app_state_store');
import ListenToAppcacheUpdates from './services/listen_to_appcache_changes.js';



var SYNC_INTERVAL = 5000;

var lastOnlineState = false;

var App = React.createClass({
	componentDidMount() {
		// Manifest change
		AppStateActions.startCacheChangeListener();
		if( AppStateStore.hasAppCacheBeenUpdated() ){
			if (confirm('A new version of this site is available. Load it?')) {
				window.location.reload();
			}
		}
		
		// IndexedDB loader
		if( AppStateStore.localDBServer().dbServer ){
			LocalDBService.loadMail();
		}


		// Syncing
		if( AppStateStore.isOnline().isOnline ){
			this._startSync();
		}


		AppStateStore.addChangeListener(this._onChange);

	},

	componentWillUnmount() {
		AppStateStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
		// IndexedDB
		if( AppStateStore.localDBServer().dbServer ){
			LocalDBService.loadMail();
		}


		// SYNC sync when online
		if( lastOnlineState !==  AppStateStore.isOnline().isOnline ){
			lastOnlineState = AppStateStore.isOnline().isOnline;
			if( AppStateStore.isOnline().isOnline ) {
				this._startSync();
			} else if( !AppStateStore.isOnline().isOnline ) {
				this._stopSync();
			}
		}
	},

	_startSync(){
		if( !this.syncIntervalId ){
			this.syncIntervalId = setInterval(() => {
				Syncer.sync(AppStateStore.localDBServer());
			}, SYNC_INTERVAL);
		}
	},

	_stopSync(){
		if( this.syncIntervalId ){
			clearInterval(this.syncIntervalId);
			this.syncIntervalId = undefined;
		}
	},

	render(){
		return (
			<div className="app">
				<Header/>
				<div className='app__content-container'>
					<div className='app__navigation-area'>
						<StoreNavigation/>
					</div>
					<div className='app__content-area'>
						<RouteHandler/>
					</div>
				</div>
			</div>
		)
	}
});

var routes = (
	<Route name="root" path="/" handler={App}>
		<DefaultRoute handler={Inbox} />
		<Route name="compose" path="/new" handler={ComposeMail}/>

		<Route name="mails" path="/:store" handler={Inbox}>
			<Route name="mail" path=":mailId" handler={MailViewer}/>
			<Route name="edit-mail" path=":mailId/edit" handler={ComposeMail}/>
		</Route>
	</Route>
);



Router.run(routes, Router.HashLocation, (Root) => {
	React.render(<Root/>, document.body);
});


