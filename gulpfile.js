var gulp = require( "gulp" ),
    supervisor = require( "gulp-supervisor" );

var nodeBrowserify = require("browserify");
var source = require("vinyl-source-stream");
var babelify = require("babelify");
var watch = require("gulp-watch");

var sass = require('gulp-sass');
var importCss = require('gulp-import-css');
var sizereport = require('gulp-sizereport');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");
var uglifycss = require('gulp-uglifycss');
var autoprefixer = require('gulp-autoprefixer');
var manifest = require('gulp-manifest');

 
gulp.task('build-client-scss', function () {
  gulp.src('./client/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(importCss())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('./server/public/app'));
});

gulp.task( "supervisor", function() {
  supervisor( "server/index.js", {
    harmony: true,
    ignore: ["server/public/app"]
  } );
} );

gulp.task("build-client-js", function() {
  var b = nodeBrowserify({
    entries: "./client/app.jsx",
    debug: true,
    transform: [babelify]
  });

  return b.bundle()
  .pipe(source("main.js"))
  .pipe(gulp.dest("server/public/app"));
});

gulp.task("watch", function() {
  watch(["client/*.jsx", "client/*.js", "client/**/*.jsx", "client/**/*.js"], function(){
    gulp.start("build-client-js");
  });

  watch(["client/*.scss", "client/**/*.scss"], function(){
    gulp.start("build-client-scss");
  });

});

gulp.task('sizereport', function () {
  return gulp.src(['./dist/*', './server/public/app/*'])
  .pipe(sizereport({
    gzip: true
  }));
});

gulp.task('compress-js', function() {
  return gulp.src('./server/public/app/*.js')
  .pipe(uglify())
  .pipe(rename(function (path) {
    path.basename += ".min";
    //      path.extname = ".md"
  }))
  .pipe(gulp.dest('dist'));
});

gulp.task('compress-css', function () {
  gulp.src('./server/public/app/*.css')
  .pipe(uglifycss({
  }))
  .pipe(rename(function (path) {
    path.basename += ".min";
    //      path.extname = ".md"
  }))
  .pipe(gulp.dest('dist'));
});

gulp.task('manifest', function(){
  gulp.src(['server/public/app/*','server/public/app/**/*'])
    .pipe(manifest({
      hash: true,
      preferOnline: true,
      network: ['http://*', 'https://*', '*'],
      cache: [
	      'http://fonts.googleapis.com/css?family=Open+Sans:300,600,400&subset=latin,latin-ext',
	      'https://fontastic.s3.amazonaws.com/4W99YizmV7sJGMZLSxZnzM/icons.css'
      ],
      filename: 'app.manifest',
      exclude: 'app.manifest'
     }))
    .pipe(gulp.dest('server/'));
});

gulp.task('makemanifest', ['compress-js', 'compress-css', 'manifest'])

gulp.task("compress", ["compress-js", "compress-css"]);


gulp.task("default", ["supervisor", "build-client-js", "build-client-scss", "watch"])
